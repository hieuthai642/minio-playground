const am = require('am')
const AWS = require('aws-sdk')
const Minio = require("minio");

process.env.S3_DEFAULT_REGION = 'us-west-1'
process.env.S3_BUCKET_NAME = 'smartec-microservice'
process.env.S3_ENDPOINT = 'http://localhost:9000'
process.env.S3_ACCESS_KEY = '123'
process.env.S3_SECRET_KEY = '12345678'


// Create bucket first
const getDefaultS3 = () => {
    return new AWS.S3({
        accessKeyId: process.env.S3_ACCESS_KEY,
        secretAccessKey: process.env.S3_SECRET_KEY,
        endpoint: process.env.S3_ENDPOINT,
        s3ForcePathStyle: true,
        signatureVersion: 'v4'
    })
}

const bucketExist = async (bucketName) => {
    try {
        const s3 = getDefaultS3()
        const isExist = await s3.headBucket({Bucket: bucketName}).promise()
        return !!isExist
    } catch (err) {
        console.log(err)
        return false
    }
}

const Khanh = {
    name: 'Khanh',
    age: 22,
    school: {
        secondary: {
            name: 'Nguyen Du High School',
            address: '18 Thong Nhat',
            phone: '123',
            isGood: true,
            friends: ['Khanh', 'Hung', 'Tan']
        },
        highschool: {
            name: 'PTnk'
        }
    }
}

const contentToWrite = {
    name: 'Hieu',
    age: 23,
    school: {
        secondary: {
            name: 'Nguyen Du',
            address: '18 Thong Nhat',
            phone: '123 123 123',
            isGood: true,
            friends: [Khanh, 'Hung', 'Tan']
        }
    }
}

const mainFn = async () => {
    const s3 = getDefaultS3()

    const isBucketExist = await bucketExist(process.env.S3_BUCKET_NAME)

    if (!isBucketExist) {
        const s3Bucket = await s3.createBucket({Bucket: process.env.S3_BUCKET_NAME}).promise()
    }

    try {
        const putParams = {Bucket: `${process.env.S3_BUCKET_NAME}/2020-01-19`, Key: 'test.json', Body: JSON.stringify(contentToWrite)};
        const resultPut = await s3.putObject(putParams).promise()
        console.log(`Successfully Uploaded data to ${process.env.S3_BUCKET_NAME}/testObject`)

        const getParams = {Bucket: process.env.S3_BUCKET_NAME, Key: '2020-01-19/test.json'}
        const resultGet = await s3.getObject(getParams).promise()
        console.log(resultGet)
    } catch (err) {
        console.log(err)
    }
}

am(mainFn)

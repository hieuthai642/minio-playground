# MINIO Playground

## Single node

- Create container
```bash
make local
```

- Stop local
```bash
make stop-local
make stop-local-hard # Remove volume
```
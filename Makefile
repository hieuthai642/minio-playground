.PHONY: local
local:
	docker-compose up --build -d

.PHONY: stop-local
stop-local:
	docker-compose down

.PHONY: stop-local-hard
stop-local-hard:
	docker-compose down -v

.PHONY: build-watch
build-watch:
	npm run build:watch